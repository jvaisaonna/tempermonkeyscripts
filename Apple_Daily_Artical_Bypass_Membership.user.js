// ==UserScript==
// @name         Apple Daily Artical Bypass Membership
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Bypass member login for visiting apply daily
// @author       JViana
// @match        https://*.appledaily.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var id = window.setTimeout(function() {}, 0);
    
    while (id--) {
        window.clearTimeout(id);
    }
})();